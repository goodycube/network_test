#!/usr/bin/env python
"""
Created on Sun Nov 24 16:10:04 2019

@author: tombu_000

run_and_tumble_network.py: Simulate/Training of an agent in a nutrient field
learning to follow the gradient in order to find resources. Initial version
should train the bacterium to remember previous nutrient concentrations and
build the action (run or tumble) on this memory.

Idea: keep last two concentration measurements as memories, plus the current
concentration measurement, plus the last action that was taken, that is:
    Input:
        - concentration (two steps earlier)
        - concentration (one step earlier)
        - concentration (this step)
        - previous action (1 for tumble, 0 for no tumble)
        
    Output:
        - concentration (one step earlier)
        - concentration (this step)
        - probability to tumble
        
    Training works as follows:
        - concentration outputs are trivial
        - calculate tumbling:
            s(x) = sigmoid(x) = 1/(1+exp(-x))
            p(tumble) = s(-(c(0)-c(-1))/c(0))* (1-prev_act)*
                        * (1-0.5*(0.5-s((c(0)+c(-2)-2c(1))^2/c(0)^2)))
            -> tumble if:
                negative gradient
                no tumbling event just before
                try to stick to areas of high curvature

"""

__author__   = "tombu_000"
__version__  = "0.0.1"

import numpy as np
import network_advanced as net
import random
import math
import world

# ======== Training definitions below ============

def sigmoid(x):
    return 1/(1+np.exp(-x))

def tumble_prob(cm2, cm1, c0, prev_act):
    gradient_factor = sigmoid(-(c0-cm1)/c0)
    history_factor = (1-prev_act)
    curvature_factor = (1-0.5*(0.5-sigmoid((c0+cm2-2*cm2)**2/(c0**2))))
    return gradient_factor * history_factor * curvature_factor

def generate_input():
    c0 = max(random.normalvariate(1.0, .5), 0.1)
    cm1 = max(random.normalvariate(1.0, .5), 0.1)
    cm2 = max(random.normalvariate(1.0, .5), 0.1)
    prev_act = int(random.randint(1, 10)/10)
    return np.array([cm2, cm1, c0, prev_act])

def expected_output(inputs):
    tp = tumble_prob(inputs[0], inputs[1], inputs[2], inputs[3])
    return np.array([inputs[1], inputs[0], tp])


# ======== Foraging definitions below ============
  
def nutrient_value(coord: np.ndarray):
    stdev = 2.0
    return np.exp(-(coord[0]-world.size/2)**2/stdev**2
                  -(coord[1]-world.size/2)**2/stdev**2)
    
class Agent:
    def __init__(self):
        self.speed = 0.4
        self.coords = np.random.rand(2) * world.size
        self.orientation = random.random() * 2 * math.pi
        self.network = net.Network([4,8,8,3])
        self.nutrient_input = nutrient_value(self.coords)
        self.path = []
        self.prev_nutrient = self.nutrient_input
        self.preprev_nutrient = self.nutrient_input
        self.prev_act = 0
        self.tumble_prob = 0.0
        
    def log_state(self):
        log_entry = [self.coords[0], self.coords[1], self.orientation,
                     self.nutrient_input, self.tumble_prob]
        self.path.append(log_entry)
        
    def employ_network(self):
        self.nutrient_input = nutrient_value(self.coords)
        inputs = np.array([self.preprev_nutrient,
                           self.prev_nutrient,
                           self.nutrient_input,
                           self.prev_act])
        network_result = self.network.feedforward_signal(inputs)
        self.preprev_nutrient = network_result[0]
        self.prev_nutrient = network_result[1]
        self.tumble_prob = network_result [2]
        
    def choose_orientation(self):
        if random.random() < self.tumble_prob:
            self.orientation = random.random() * 2 * math.pi
            self.prev_act = 1
        else:
            self.prev_act = 0
            
    def move(self):
        self.coords = (self.coords + 
                       self.speed * np.array([np.cos(self.orientation),
                                              np.sin(self.orientation)]))
        self.coords = world.handle_boundaries(self.coords)
        
    def make_step(self):
        self.log_state()
        self.employ_network()
        self.choose_orientation()
        self.move()
        
    def make_path(self, step_count: int):
        for counter in range(step_count):
            self.make_step()
        self.log_state()
        
    def export_path(self):
        with open('sample_path.txt', 'w') as file:
            for log_entry in self.path:
                file.write('\t'.join([str(part) for part in log_entry]) + '\n')
                
            
        

bact = Agent()

for i in range(10000):
    training_input = generate_input()
    training_output = expected_output(training_input)
    bact.network.train_network(training_input, training_output)
    
bact.make_path(100)
bact.export_path()












