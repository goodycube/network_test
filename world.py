#!/usr/bin/env python
"""
Created on Mon Nov 25 15:31:02 2019

@author: tombu_000

world.py: A basic module that contains information about the world/environment
where a population resides, grouping everything that would exist independent of
any inhabitants of the world

"""

__author__   = "tombu_000"
__version__  = "0.0.1"

import numpy as np


size = 10.0 # size of the map, by default it is size*size large
_map_type = 'periodic'   # how the boundaries should be treated 



_unit_length = 1.0   # in case the world needs to be rescaled

def set_unit_length(new_length: float):
    '''
    Function to rescale all lengths in the world.
    '''
    
    conversion_factor = new_length/_unit_length
    size = size * conversion_factor
    
    _unit_length = _unit_length * conversion_factor
    
def set_map_type(new_type: str):
    valid_types = ['periodic', 'reflective', 'absorbing']
    if new_type in valid_types:
        _map_type = new_type
    else:
        raise Exception('Map type {} is not a valid type.'.format(new_type))
        
def handle_boundaries(coords: np.ndarray) -> np.ndarray:
    if _map_type == 'periodic':
        return coords % size
    else:
        raise Exception(('Boundary handling for map ' +
                         'type {} not implemented').format(_map_type))
    
    
    
    
    
    
    
    
    
    
    