#!/usr/bin/env python
"""
Created on Thu Nov 21 10:24:49 2019

@author: tombu_000

[filename].py: [Description]

"""

__author__   = "tombu_000"
__version__  = "0.0.1"

import numpy as np

def sigmoid_function(x):
    return 1/(1+np.exp(-x))

def sigmoid_function_derivative(x):
    return sigmoid_function(x) * (1-sigmoid_function(x))

class Layer:
    def __init__(self, width: int):
        self.output = np.zeros(width, dtype = float)
        self.layer_width = width
        
    def backprop_product(self, vec1: np.ndarray, vec2: np.ndarray):
        '''
        Map a vector of factors (vec1) to a second vector of the same 
        length (vec2), by converting it to a diagonal matrix
        '''
        
        aux_mat = np.identity(len(vec1)) * np.outer(np.ones(len(vec1)), vec1)
        return aux_mat.dot(vec2)
        
        
class Processing_Layer(Layer):
    def __init__(self, width: int, preceeding_width: int):
        super().__init__(width)
        self.signal = np.zeros(width, dtype = float)
        self.weights = np.random.normal(size = (width, preceeding_width+1))
            # weights[0] accesses the weights applying to the 0th neuron in
            # this layer. weights[i][-1] represents the bias of the neuron i
        self.gradient_factor = np.zeros(width)
        
    def aggregate_signal(self, inputs: np.ndarray):
        augmented_inputs = np.append(inputs, 1.0)
        self.signal = self.weights.dot(augmented_inputs)
        
    def activate_signal(self):
        self.output = self.activation_function(self.signal)
        
    def progress_signal(self, inputs: np.ndarray):
        self.aggregate_signal(inputs)
        self.activate_signal()
        
    def activation_function(self, x):
        return sigmoid_function(x)
    
    def activation_function_derivative(self, x):
        return sigmoid_function_derivative(x)
    
    def overwrite_weights(self, new_weights: np.ndarray):
        if self.weights.shape != new_weights.shape:
            raise Exception(("Trying to replace weight matrix with shape {} " +
                             "with a matrix with distinct shape {}.").format(
                                     self.weights.shape, new_weights.shape))
        else:
            self.weights = new_weights.astype(float)
            
    def weights_gradient(self, preceding_activations: np.ndarray):
        return np.outer(self.gradient_factor, preceding_activations)
                    
class Input_Layer(Layer):
    def __init__(self, width: int):
        super().__init__(width)
        
    def progress_signal(self, inputs: np.ndarray):
        self.output = inputs
        
        
class Hidden_Layer(Processing_Layer):
    def __init__(self, width: int, preceeding_width: int):
        super().__init__(width, preceeding_width)
        
    def calculate_aux_gradient(self, succeeding_weights: np.ndarray,
                               succeeding_gradient_factor:np.ndarray):
        self.gradient_factor = self.backprop_product(
                np.append(self.activation_function_derivative(self.signal), 1),
                succeeding_weights.transpose().dot(succeeding_gradient_factor))
        self.gradient_factor = np.delete(self.gradient_factor, -1)
        
class Output_Layer(Processing_Layer):
    def __init__(self, width: int, preceeding_width: int):
        super().__init__(width, preceeding_width)
        
    def calculate_aux_gradient(self, expected_outputs: np.ndarray):
        self.gradient_factor = self.backprop_product(
                self.activation_function_derivative(self.signal),
                self.loss_function_gradient(expected_outputs))
    
    def loss_function(self, expected_outputs: np.ndarray):
        '''
        Mean Square Error: err = sum((expect_i - obained_i)^2)
        '''
        
        diff = expected_outputs - self.output
        return diff.dot(diff)
        
    def loss_function_gradient(self, expected_outputs: np.ndarray) -> np.ndarray:
        return -2 * (expected_outputs - self.output)
        
class Network:
    def __init__(self, nodes: list, learning_rate: float = 0.1):
        self.layers = []
        self.learning_rate = learning_rate
        
        for layer_index in range(len(nodes)):
            if layer_index == 0:
                self.layers.append(Input_Layer(nodes[0]))                
            elif 0 < layer_index < len(nodes) - 1:
                self.layers.append(Hidden_Layer(nodes[layer_index],
                                                    nodes[layer_index - 1]))
            elif layer_index == len(nodes) - 1:
                self.layers.append(Output_Layer(nodes[layer_index],
                                                nodes[layer_index - 1]))
                
    def feedforward_signal(self, inputs: np.ndarray) -> np.ndarray:
        temp_signal = inputs
        for layer in self.layers:
            layer.progress_signal(temp_signal)
            temp_signal = layer.output
        return temp_signal
    
    def backpropagate(self, expected_outputs: np.ndarray):
        for layer in reversed(self.layers):
            if type(layer) == Output_Layer:
                layer.calculate_aux_gradient(expected_outputs)
            elif type(layer) == Hidden_Layer:
                layer.calculate_aux_gradient(
                        self.layers[self.layers.index(layer)+1].weights,
                        self.layers[self.layers.index(layer)+1].gradient_factor
                        )
            elif type(layer) == Input_Layer:
                pass
            else:
                raise Exception("Invalid object encountered in network: " +
                                "layer type expected, got " +
                                "{}".format(type(layer)))
                
    def correct_weights(self):
        for layer in self.layers:
            if type(layer) == Input_Layer:
                pass
            elif type(layer) == Hidden_Layer or type(layer) == Output_Layer:
                augmented_preceding_outputs = np.append(
                        self.layers[self.layers.index(layer) - 1].output, 1)
                layer_correction = self.learning_rate * layer.weights_gradient(
                        augmented_preceding_outputs)
                layer.weights = layer.weights - layer_correction
                
    def train_network(self, inputs: np.ndarray, exp_outputs: np.ndarray):
        self.feedforward_signal(inputs)
        self.backpropagate(exp_outputs)
        self.correct_weights()
        
    def training_session(self, training_data: list, training_units: int = 0):
        if training_units == 0:
            for dataset in training_data:
                self.train_network(dataset['input'], dataset['exp_output'])
        else:
            import random
            for iterator in range(training_units):
                dataset = random.choice(training_data)
                self.train_network(dataset['input'], dataset['exp_output'])
        
                
        
        
        
        
#    
#my_network = Network([2,1])
##
##my_weights = np.array([[0,0,0]])
##
##my_network.layers[1].overwrite_weights(np.array([[0,0,0],[0,0,0]]))
##my_network.layers[2].overwrite_weights(my_weights)
#
#trainingdata = [
#        {'input' : np.array([-2, -1]), 'exp_output' : np.array([1.])},
#        {'input' : np.array([25, 6]), 'exp_output' : np.array([0.])},
#        {'input' : np.array([17, 4]), 'exp_output' : np.array([0.])},
#        {'input' : np.array([-15, -6]), 'exp_output' : np.array([1.])}
#        ]
#
#my_network.training_session(trainingdata, 1000)
#
##print(my_network.layers[1].weights, my_network.layers[2].weights, sep= '\n\n', end = '\n\n\n')
#print(my_network.feedforward_signal(np.array([-7,-3])))
#print(my_network.layers[-1].weights)
#



























